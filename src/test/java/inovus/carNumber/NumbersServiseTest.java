package inovus.carNumber;

import inovus.carNumber.domain.CarNumber;
import inovus.carNumber.repository.NumberRepository;
import inovus.carNumber.service.NumberService;
import org.junit.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class NumbersServiseTest {


    @Test
    public void creationRepositoryTest() {
        NumberRepository repository = new NumberRepository();
        int result = repository.size();
        assertEquals(0, result);

    }

    @Test
    public void sizeWithOneTest() {
        NumberRepository repository = new NumberRepository();
        repository.add(new CarNumber("А", 0, "А", "А"));
        int result = repository.size();
        assertEquals(1, result);

    }

    @Test
    public void nextTest() {
        NumberRepository repository = new NumberRepository();
        NumberService service = new NumberService(repository);

        String result = service.next();
        CarNumber expected = new CarNumber("А", 1, "А", "А");
        assertEquals(expected.toString(), result);

    }

    @Test
    public void randomTest() {
        NumberRepository repository = new NumberRepository();
        NumberService service = new NumberService(repository);

        service.random();
        int result = repository.size();
        assertEquals(1, result);

    }

    @Test
    public void nextWithOneTest() {
        NumberRepository repository = new NumberRepository();
        NumberService service = new NumberService(repository);
        repository.add(new CarNumber("А", 1, "А", "А"));

        String result = service.next();
        CarNumber expected = new CarNumber("А", 2, "А", "А");
        assertEquals(expected.toString(), result);

    }

    @Test
    public void randomAfterNextTest() {
        NumberRepository repository = new NumberRepository();
        NumberService service = new NumberService(repository);
        service.next();
        service.random();
        int result = repository.size();
        assertEquals(2, result);

    }


}
