package inovus.carNumber.repository;


import inovus.carNumber.domain.CarNumber;
import org.springframework.stereotype.Repository;

import java.util.HashSet;
import java.util.Set;

@Repository
public class NumberRepository implements NumberRepositoryImpl {


    private Set<CarNumber> carNumbers = new HashSet<>();

    @Override
    public void add(CarNumber carNumber) {
        carNumbers.add(carNumber);
    }

    @Override
    public boolean contains(CarNumber carNumber) {
        return carNumbers.contains(carNumber);
    }

    @Override
    public int size() {
        return carNumbers.size();
    }


}
