package inovus.carNumber.repository;


import inovus.carNumber.domain.CarNumber;

public interface NumberRepositoryImpl {
    void add(CarNumber carNumber);

    boolean contains(CarNumber carNumber);

    int size();


}
