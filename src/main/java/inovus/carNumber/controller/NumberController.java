package inovus.carNumber.controller;

import inovus.carNumber.repository.NumberRepository;
import inovus.carNumber.service.NumberService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping()
public class NumberController {
    private final NumberService numberService;

    public NumberController() {
        this.numberService = new NumberService(new NumberRepository());
    }

    @GetMapping("/next")
    public String next() {
        return numberService.next();
    }

    @GetMapping("/random")
    public String random() {
        return numberService.random();
    }

}
