package inovus.carNumber.service;

import inovus.carNumber.domain.CarNumber;
import inovus.carNumber.exception.ItWasLastNumberException;
import inovus.carNumber.repository.NumberRepository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class NumberService {
    private final NumberRepository numberRepository;


    public NumberService(NumberRepository numberRepository) {
        if (numberRepository == null) throw new IllegalArgumentException();
        this.numberRepository = numberRepository;
    }


    private List<String> alphabet = new ArrayList<>(List.of("А", "В", "Е", "К", "М", "Н", "О", "Р", "С", "Т", "У", "Х"));
    private CarNumber lastCarNumber = new CarNumber("А", 0, "А", "А");


    private String randomChar() {
        return alphabet.get((int) (Math.random() * alphabet.size()));
    }

    private int randomNumber() {
        int numberTo = 999;
        int numberFrom = 0;
        return numberFrom + (int) (Math.random() * numberTo);
    }

    public String next() {
        CarNumber newCarNumber;
        do {
            newCarNumber = new CarNumber(lastCarNumber.getFirstChar(), lastCarNumber.getNumbers(), lastCarNumber.getSecondChar(), lastCarNumber.getThirdChar());
            if (newCarNumber.getNumbers() != 999) {
                newCarNumber.setNumbers(newCarNumber.getNumbers() + 1);

            } else if (!newCarNumber.getThirdChar().equals("X")) {
                String aChar = newCarNumber.getThirdChar();
                int i = alphabet.indexOf(aChar);
                newCarNumber.setThirdChar(alphabet.get(i + 1));
                newCarNumber.setNumbers(0);

            } else if (!newCarNumber.getSecondChar().equals("X")) {
                String aChar = newCarNumber.getSecondChar();
                int i = alphabet.indexOf(aChar);
                newCarNumber.setSecondChar(alphabet.get(i + 1));
                newCarNumber.setNumbers(0);
                newCarNumber.setThirdChar("A");

            } else if (!newCarNumber.getFirstChar().equals("X")) {
                String aChar = newCarNumber.getFirstChar();
                int i = alphabet.indexOf(aChar);
                newCarNumber.setFirstChar(alphabet.get(i + 1));
                newCarNumber.setNumbers(0);
                newCarNumber.setThirdChar("A");
                newCarNumber.setSecondChar("A");
            } else {
                throw new ItWasLastNumberException(newCarNumber.toString());
            }
            lastCarNumber = newCarNumber;
        } while (numberRepository.contains(newCarNumber));


        numberRepository.add(newCarNumber);
        return newCarNumber.toString();

    }


    public String random() {
        CarNumber newCarNumber;
        do {
            newCarNumber = new CarNumber(randomChar(), randomNumber(), randomChar(), randomChar());
        } while (numberRepository.contains(newCarNumber));

        numberRepository.add(newCarNumber);
        lastCarNumber = newCarNumber;
        return lastCarNumber.toString();
    }


}
