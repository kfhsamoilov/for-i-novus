package inovus.carNumber.exception;

public class ItWasLastNumberException extends RuntimeException {
    public ItWasLastNumberException() {
    }

    public ItWasLastNumberException(String message) {
        super(message);
    }

    public ItWasLastNumberException(String message, Throwable cause) {
        super(message, cause);
    }

    public ItWasLastNumberException(Throwable cause) {
        super(cause);
    }

    protected ItWasLastNumberException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
