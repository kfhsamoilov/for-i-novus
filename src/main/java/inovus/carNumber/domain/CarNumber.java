package inovus.carNumber.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Formatter;
import java.util.Objects;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CarNumber {
    private String firstChar;
    private int numbers;
    private String secondChar;
    private String thirdChar;
    public final String region = "116 RUS";

    public String toString() {
        Formatter formatter = new Formatter();
        return firstChar + " " + formatter.format("%03d%n", numbers) + " " + secondChar + thirdChar + " " + region;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof CarNumber)) return false;
        CarNumber carNumber = (CarNumber) o;
        return getNumbers() == carNumber.getNumbers() &&
                Objects.equals(getFirstChar(), carNumber.getFirstChar()) &&
                Objects.equals(getSecondChar(), carNumber.getSecondChar()) &&
                Objects.equals(getThirdChar(), carNumber.getThirdChar()) &&
                Objects.equals(getRegion(), carNumber.getRegion());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getFirstChar(), getNumbers(), getSecondChar(), getThirdChar(), getRegion());
    }
}
